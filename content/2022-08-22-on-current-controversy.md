+++
title = "On Current Controversy"
date = "2022-08-22T21:33:00Z"
draft = true 
[taxonomies]
tags = ["PINE64","PinePhone",]
categories = ["commentary"]
authors = ["peter"]
+++
_In place of a weekly update I really don't want to write right now[^1], let's have a brief commentary on the controversy around PINE64._

<!-- more -->
If you missed this, here are the two main pieces of writing that are at the heart of this:

It's the announcement of beloved Community Member Martijn Braam:
* Martijn Braam: [Why I left PINE64](https://blog.brixit.nl/why-i-left-pine64). 
  * [HN Comment Thread](https://news.ycombinator.com/item?id=32494659),
  * [r/PINE64official Comment Thread](https://www.reddit.com/r/PINE64official/comments/wqpdlq/martijn_braam_stepping_away_from_pine64/)

and then the response by PINE64 Community Manager Marek Kraus (whom you may know by his nicks Gamiee and Gamelaster):
* Marek Kraus for PINE64: [A response to Martijn’s blog](https://www.pine64.org/2022/08/18/a-response-to-martijns-blog/)
  * [HN Comment Thread](https://news.ycombinator.com/item?id=32507912),
  * [r/PINE64official](https://www.reddit.com/r/PINE64official/comments/wrld5f/pine64s_response_to_martijns_blog/)

There's been more,[^2] but these are the main posts that need to be mentioned and that you should have read so that you can then point out the flaws in my summaries below.

### Martijn's post

Martijn (who gladly will stay involved with postmarketOS and has worked on numerous projects that pushed the Linux Phone experience forward)[^3] describes why he is leaving: He alledges that after the End of Community Editions and by shipping Manjaro ARM with Plasma Mobile since then (on both the PinePhone and the PinePhone Pro), Manjaro has gained more influence in PINE64's decisions to the harm of the wider development community:

> PINE64 cares only about Manjaro, and Manjaro does not care about working with any other distributions.

This would harm the development of new PINE64 Linux Devices, like the PineNote and the PinePhone Pro:

> Many of PINE64's new devices, such as the PinePhone Pro, PineNote, and others, have few to no developers working on the software — a potential death blow for PINE64's model of relying on the community to build the software

He also cites what made him decide to leave: The fact that the recent PineBook Pro batch did not ship with Tow-Boot on the SPI chip, and that the Tow-Boot developers only learned this from customers of these laptops. 

### Marek's response

Gamiee starts by stating that Martijn leaving is a significant loss to the project and, on a private level, a sad state of affairs. He then goes through the whole SPI on RK3399 devices story, and states that PINE64 is in fact listening to community members. The key quote IMHO here is:

>  Differences of opinion and unwillingness to listen are two different things and cannot be equated.

He also goes into a future bounty feature of the DevZone and that progress with that has been slower than hoped during the past year.

What Marek did not adress, though, was the relationship to Manjaro, or the alledged slower progress with the PinePhone Pro due to few developers working on it.

### Now: What to make of this?

I think this is a difficult situation. To have a seasoned Community Developer leave a project is sad, and Martijn IMHO brings up some very valid points. I also miss the Community Editions, and wish they had continued or been reintroduced for the PinePhone Pro. 

To me, the most important point that PINE64 needs to pay attention to, is that Martijn left because he did not feel listened too. This is something that should not happen, and it might be a resource issue on PINE64's side. With the community's success, and the addition of more and more, and in itself more complex devices, the position of a Community Manager may no longer be a job that can reasonable done by one person alone. This will add cost, but I believe that there's less harm in a slight price increase that manages to keep people more happy. _Please note:_ This is the long-term thing. The short term thing would have be a simple apology. It does not cost nothing, and sadly Marek missed the best opportunity for a public apology. But: It's not a lost cause. Reaching out to contributors privately, asking how they feel, could still accomplish this.

What's - in my humble opinion is not the solution - is the following that has been demanded by Drew DeVault:

> * Rescind their singular commitment to Manjaro.

Now, if you've followed my late blog posts, this may surprise you, as you have noticed that I am not a fan of Manjaro. In fact, I've never been convinced that snapshotting a roling release distro is a good idea for the security issues that possibly brings alone; I really don't like that they [still ship WIP](https://dont-ship.it/); and I dislike their excessive branding (yes, I believe the [Please don’t theme our apps](https://stopthemingmy.app/) crowd has a valid point), everything and not respect upstream demands like and I've had some fun (I do look back fondly at Manjaro Lomiri) and some not so fun experiences with Manjaro on the PinePhone (like breakage on updates) and other PINE64 devices.

But as long as it is easy enough, to use something else - which it has always been on the PinePhone (as long as you have a microSD card) and the PinePhone Pro (now that Tow-Boot is preinstalled there, it is), it is fine to have something shipped pre-installed that I am not particularly enthusiastic about. As long as what comes pre-installed is more useful than a pure factory test image, which I am sure Manjaro Plasma Mobile certainly is in my mind (and boy, has Plasma Mobile improved since Beta Edition started shipping!), it's a good enough choice.

I also recognize that Manjaro is not only popular in the wider Linux User crowd, but that they are also quite good at dealing with OEMs - there's quite a number of hardware partners that ship Manjaro preinstalled. So while Manjaro may not be the best at renewing TLS certs, they clearly know, as a business, how to make deals with other businesses and keep these happy long term - which is something that's likely quite important for the corporate side of PINE64, PINEStore Ltd.
 
> * Re-instate community editions and expand the program.

Yes, more variety would be nice, but we need to consider that this would add complexity and thus cost. It would also make the very limited support PINEStore Ltd. offers, way more difficult. I think we, as an enthusiast community, need to recognize that there's been quite the component price increase since "component shortage started, that PINE64 did not pass on for the PinePhone - and for other products, pricing has been raised way less than cost have.

I doubt that Manjaro is receiving the 10 USD per device - I would be it's much less - that was given to projects during the community editions, and I would welcome it if PINE64 (all parts of PINE64 involved) and Manjaro (plus potential, additional involved parties) would explain better, what the details of this arrangement are. Transparency can likely go a long way here to help with Community acceptance, despite being difficult for businesses that understandably don't want to disclose more than they have to.

### The bigger picture

There's been other criticsm of PINE64 lately, that I also want to opine on.


[^1]: Blame Chris Titus and his IMHO incredibly bad and  harmful take on the PinePhone Pro, where he sinks the entire GNU-like Linux Mobile ecosystem just because hardware enablement on the PinePhone Pro is not done yet. That said, I'll attempt to include the pasts weeks news into next mondays link-list best I can.

[^2]: 

[^3]: The 
