+++
title = "Weekly GNU-like Mobile Linux Update (8/2023): Snapdragon 845 improvements and usage reports"
draft = false
date = "2023-02-26T20:00:00Z"
[taxonomies]
tags = ["Sailfish OS","Nemo Mobile","Maui","Lapdock",]
categories = ["weekly update"]
authors = ["peter"]
[extra]
author_extra = " (with friendly assistance from plata's awesome script)"
+++

Don't worry, there's more: Reports by NemoMobile and Maui, an easier way to playback DRM content on 64-bit ARM on Manjaro and Arch, plus the usual weekly guests.

<!-- more -->
_Commentary in italics._

### Software progress

#### GNOME ecosystem
- This Week in GNOME: [#84 Polished Circle](https://thisweek.gnome.org/posts/2023/02/twig-84/)
- r/MobileLinux: [[Mock-up] Bottom Sheet Dialogs by Tobias Bernard](https://www.reddit.com/r/mobilelinux/comments/118modb/mockup_bottom_sheet_dialogs_by_tobias_bernard/)

#### Plasma ecosystem	
- Nate Graham: [This week in KDE: even better multi-monitor](https://pointieststick.com/2023/02/24/this-week-in-kde-even-better-multi-monitor/)
- MauiKit.org: [Maui Report 21](https://mauikit.org/blog/maui-report-21/)
- KDE Announcements: [KDE Plasma 5.27.1, Bugfix Release for February](https://kde.org/announcements/plasma/5/5.27.1/)

#### Sailfish OS
- [Sailfish Community News, 23rd February, CLAT](https://forum.sailfishos.org/t/sailfish-community-news-23rd-february-clat/14752)

#### Nemo Mobile
- Nemo Mobile UX team: [Nemomobile in February 2023](https://nemomobile.net/pages/nemomobile-in-february-2023/)
- neochapay on twitter: [New images of #nemomobile #glacierUX https://img.nemomobile.net/2023.05/ for #pinephone #pinephonepro @thepine64 Rework initialization fixed messages and keyboard! Welcome to testing ^_^ And welcome to channel https://t.me/NemoMobile](https://nitter.net/neochapay/status/1629472625003986949#m)

#### Linux
- CNX Software: [Linux 6.2 release - Main changes, Arm, RISC-V, and MIPS architectures](https://www.cnx-software.com/2023/02/20/linux-6-2-release-main-changes-arm-risc-v-and-mips-architectures/)

#### Non-Linux
- Lup Yuen: [(Possibly) Emulate PinePhone with Unicorn Emulator](https://lupyuen.github.io/articles/unicorn?1)

#### Matrix
- Matrix.org: [This Week in Matrix 2023-02-24](https://matrix.org/blog/2023/02/24/this-week-in-matrix-2023-02-24)
- nheko (Matrix): [v0.11.3](https://github.com/Nheko-Reborn/nheko/releases/tag/v0.11.3), nheko (Matrix): [v0.11.2](https://github.com/Nheko-Reborn/nheko/releases/tag/v0.11.2)

#### Worth noting
- [caleb: "type-c data and power role switching sorta almost kinda working on SDM845 #LinuxMobile. this is the fabled "OTG" support (please stop calling it that). Still a lot of work to do, but i thought I'd give y'all a glimpse ;p…" - Fosstodon](https://fosstodon.org/@calebccff/109921403500131565)
- [Sebastian Krzyszkowiak: "Just flashed my phone with an OS image that was created on the same phone a few minutes earlier 😜 It works! #librem5" - Librem Social](https://social.librem.one/@dos/109913291293924402)
- [Cédric Bellegarde: "Just sent my MR for dozing support…" - FLOSS.social](https://floss.social/@gnumdk/109918490451898598)
- [Manjaro ARM: "Thanks to @mogwai (and the @Raspberry_Pi), there is now a nice Widevine solution for aarch64/arm64. In #ManjaroARM and #ArchLinuxARM you can now install glibc-widevine and widevine-aarch64 from the #AUR to get access to DRM content on Chromium and Firefox.…" - Fosstodon](https://fosstodon.org/@ManjaroARM/109910550799422889). _Digital Restrictions Management, everybody!_
- [Clayton: "Some people make shiny new changes to #postmarketOS, like @calebccff awesome pbsplash. I, on the other hand, like spending time that @igalia pays me to work on #postmarketOS by making super unglamorous changes like rewriting our tool for generating and configuring initramfs images...…" - Free Radical](https://freeradical.zone/@craftyguy/109900278070777568)

### Worth reading
- David Field: [Postmarketos on a Oneplus6 is a glimpse of how the future might look – Tech Blog](https://tech.davidfield.co.uk/2023/02/20/postmarketos-on-a-oneplus6-is-a-glimpse-of-how-the-future-might-look/). _Report about how the OnePlus 6 works with Plasma Mobile._
- Liliputing: [Now you can run some Windows apps on Ubuntu Touch phones (emphasis on some)](https://liliputing.com/now-you-can-run-some-windows-apps-on-ubuntu-touch-phones-emphasis-on-some/)
- Jakub Steiner: [Flathub Brand Refresh](https://blog.jimmac.eu/2023/flathub-brand-refresh/)
- PineGuild: [Manjaro with Phosh Beta29 (PinePhone / PinePhonePro)](https://pineguild.com/manjaro-with-phosh-beta29-pinephone-pinephonepro/)
- LinuxPhoneApps: [New apps of LinuxPhoneApps.org, Q4/2022](https://linuxphoneapps.org/blog/new-listed-apps-q4-2022/) _Finally!_

### Worth watching
- CyberPunked: [Ubuntu Touch 20.04 - Google Pixel 3a - Waydroid (2023-02-22)](https://www.youtube.com/watch?v=ltxj8YBoTMU)
- CyberPunked: [Ubuntu Touch Wireless External Display - Google Pixel 3a](https://www.youtube.com/watch?v=ValkTii8KLg)
- Purism: [Introducing the Lapdock Kit](https://www.youtube.com/watch?v=Ac7nhzYYFcw)
- Timur Moziev: [postmarketOS on Samsung Galaxy Note 10.1 (GT-N8000) Test Run](https://www.youtube.com/watch?v=WcWIYvnAdhE)
- DHOCNET: [Introducing PLUX-XR2 - Cheap Pocket Linux Powered by postmarketOS | Xiaomi Redmi 2 Prime](https://www.youtube.com/watch?v=hyzRogmikMA)


### Thanks
Huge thanks again to Plata for [the nifty set of Python scripts](https://framagit.org/linmob/linmob.frama.io/-/merge_requests/5) that speeds up collecting links from feeds by a lot.

### Something missing? Want to contribute?
If your project's cool story (or your awesome video or nifty blog post or ...) is missing and you don't want that to happen again, please just put it into [the hedgedoc pad](https://pad.hacc.space/7yCLy5a9QyOLWusIFiTt9A?edit) for the next one! Since I am collecting many things there, this get's you early access if you will ;-) __If you just stumble on a thing, please put it in there too - all help is appreciated!__

